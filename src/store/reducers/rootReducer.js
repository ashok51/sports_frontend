// import authReducer from "./authReducer";
import  blogReducer  from "./blogReducer";
import { combineReducers } from "redux";

const rootReducers = combineReducers({
  // auth: authReducer,
  blog: blogReducer
});

export default rootReducers;
