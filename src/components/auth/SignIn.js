import React, { Component } from "react";
import { Form, Input, Button, Checkbox, Row, Col } from "antd";
import Cookies from "js-cookie";
import { Redirect } from "react-router-dom";

class SignIn extends Component {
  constructor(props) {
    super();
  }
  state = {
    email: "",
    password: ""
  };

  handleSubmit = e => {
    const requestOptions = {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.state)
    };

    const extractToken = headers => {
      //headers are arguments from backend
      let token = {
        "access-token": "",
        client: "",
        uid: "",
        isAuthenticated: false,
        expiry: ""
      };
      if (headers.get("client")) {
        token["access-token"] = headers.get("access-token");
        token.client = headers.get("client");
        token.uid = headers.get("uid");
        token.isAuthenticated = true;
        token.expiry = headers.get("expiry");
      }
      return token;
    };

    fetch("http://localhost:3000/auth/sign_in", requestOptions).then(
      response => {
        const token = extractToken(response.headers);
        if (response.status === 200) {
          Cookies.set("accesstoken", token["access-token"], {
            expires: 7,
            path: "/"
          });
          Cookies.set("client", token["client"], { expires: 7, path: "/" });
          Cookies.set("isAuthenticated", token["isAuthenticated"], {
            expires: 7,
            path: "/"
          });
          Cookies.set("uid", token["uid"], {
            expires: 7,
            path: "/"
          });
          Cookies.set("expiry", token["expiry"], {
            expires: 7,
            path: "/"
          });
          console.log("Signed In successfully!");
          this.props.history.push("/");
        } else console.log("Sign in failed");
      }
    );
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const layout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 16
      }
    };
    const tailLayout = {
      wrapperCol: {
        offset: 8,
        span: 16
      }
    };

    const onFinish = values => {
      console.log("Success:", values);
    };

    const onFinishFailed = errorInfo => {
      console.log("Failed:", errorInfo);
    };

    return (
      <div
        className="container"
        style={{
          width: "50%",
          margin: "100px",
          height: "50%",
          padding: "0.75em"
        }}
      >
        <h2>Login</h2>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="Username"
            name="username"
            rules={[
              {
                required: true,
                message: "Please input your username!"
              }
            ]}
          >
            <Input onChange={this.handleChange.bind(this)} name="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!"
              }
            ]}
          >
            <Input.Password
              onChange={this.handleChange.bind(this)}
              name="password"
            />
          </Form.Item>

          <Form.Item {...tailLayout} name="remember" valuePropName="checked">
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleSubmit.bind(this)}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default SignIn;