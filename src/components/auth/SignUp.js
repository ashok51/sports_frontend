import React, { Component } from "react";
import { Form, Input, Button, Checkbox, Row, Col } from "antd";
import Axios from "axios";

class Signup extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    email: "",
    password: "",
    first_name: "",
    last_name: ""
  };

  async handleSubmit() {
    const params = {
      email: this.state.email,
      password: this.state.password,
      first_name: this.state.first_name,
      last_name: this.state.last_name
    };
    let response = await Axios.get("http://localhost:3000/auth/sign_up", {
      params
    });
    debugger;
    if (response.status == 200) {
      console.log("signed in successfully");
      this.props.history.push("/");
    } else {
      console.log("sign in failed");
    }
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const layout = {
      labelCol: {
        span: 8
      },
      wrapperCol: {
        span: 16
      }
    };
    const tailLayout = {
      wrapperCol: {
        offset: 8,
        span: 16
      }
    };

    const onFinish = values => {
      console.log("Success:", values);
    };

    const onFinishFailed = errorInfo => {
      console.log("Failed:", errorInfo);
    };

    return (
      <div
        className="container"
        style={{
          width: "50%",
          margin: "100px",
          height: "50%",
          padding: "0.75em"
        }}
      >
        <h3>Create your Account!</h3>
        <Form
          {...layout}
          name="basic"
          initialValues={{
            remember: true
          }}
          onSubmit={this.handleSubmit.bind(this)}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
        >
          <Form.Item
            label="First Name"
            name="first_name"
            rules={[
              {
                required: true,
                message: "Please input your First Name!"
              }
            ]}
          >
            <Input onChange={this.handleChange.bind(this)} name="first_name" />
          </Form.Item>

          <Form.Item label="Last Name">
            <Input onChange={this.handleChange.bind(this)} name="last_name" />
          </Form.Item>

          <Form.Item
            label="email"
            name="email"
            rules={[
              {
                required: true,
                message: "Please input your email!"
              }
            ]}
          >
            <Input onChange={this.handleChange.bind(this)} name="email" />
          </Form.Item>

          <Form.Item
            label="Password"
            name="password"
            rules={[
              {
                required: true,
                message: "Please input your password!"
              }
            ]}
          >
            <Input.Password
              onChange={this.handleChange.bind(this)}
              name="password"
            />
          </Form.Item>

          <Form.Item {...tailLayout}>
            <Button
              type="primary"
              htmlType="submit"
              onClick={this.handleSubmit.bind(this)}
            >
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default Signup;
