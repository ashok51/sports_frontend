import React, { Component } from "react";
import Cookies from "js-cookie";
import axios from "axios";
import { Link } from "react-router-dom";
import { authHeader } from "../../helpers/auth-headers";

class SignOut extends Component {
  constructor(props) {
    super(props);
  }

  async onHandelClick(e) {
    e.preventDefault();
    let res = await axios.delete("http://localhost:3000/auth/sign_out", {
      headers: authHeader()
    });
    debugger;
    if (res.status == 200) {
      Cookies.remove("accesstoken", { path: "/" });
      Cookies.remove("client", { path: "/" });
      Cookies.remove("isAuthenticated", { path: "/" });
      Cookies.remove("expiry", { path: "/" });
      Cookies.set("isAuthenticated", false);
      Cookies.remove("uid", { path: "/" });
      window.location.href = "/";
    }
  }
  render() {
    return <Link onClick={this.onHandelClick.bind(this)}>Logout</Link>;
  }
}

export default SignOut;
