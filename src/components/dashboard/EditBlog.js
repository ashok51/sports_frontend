import React, { Component } from "react";
import { Card, Row, Col, Modal, Layout, Breadcrumb, Input, Button } from "antd";
import axios from "axios";
import { authHeader } from "../../helpers/auth-headers";

const { Header, Footer, Sider, Content } = Layout;
const { TextArea } = Input;

class EditBlog extends Component {
  constructor(props) {
    super();
  }
  state = {
    id: "",
    title: "",
    body: ""
  };

  componentDidMount() {
    this.setState({
      title: this.props.location.state.title,
      body: this.props.location.state.body,
      id: this.props.location.state.id
    });
  }

  handleChangeTitle = e => {
    this.setState({
      title: e.target.value //yesma pani blog ko title ra body update hunxa
    });
  };

  handleChangeBody = e => {
    this.setState({
      body: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    const blog = {
      id: this.state.id,
      title: this.state.title,
      body: this.state.body
    };
    axios
      .put(`http://localhost:3000/blogs/${this.state.id}`, blog, {
        headers: authHeader()
      })
      .then(response => {
        if (response.status == 200) {
          console.log("Successfully Updated");
          this.props.history.push("/");
        } else {
          console.log("cant updated");
        }
      });
  };

  handleDelete = e => {
    axios
      .delete(`http://localhost:3000/blogs/${this.state.id}`, {
        headers: authHeader()
      })
      .then(response => {
        debugger;
      })
      .then((window.location.href = "/"));
  };

  render() {
    return (
      <div>
        <Layout>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Edit Blog</Breadcrumb.Item>
            </Breadcrumb>
            <div className="site-layout-content">
              <div className="edit-blog-input">
                <TextArea
                  name="title"
                  autoSize
                  value={this.state.title}
                  onChange={this.handleChangeTitle}
                />

                <TextArea
                  name="body"
                  value={this.state.body}
                  placeholder="Controlled autosize"
                  autoSize={{ minRows: 3, maxRows: 5 }}
                  style={{ marginTop: "10%" }}
                  onChange={this.handleChangeBody}
                />
                <Button
                  type="primary"
                  style={{ marginTop: "2%" }}
                  onClick={this.handleSubmit.bind(this.state.id)}
                >
                  Edit Post
                </Button>
                <Button
                  type="danger"
                  style={{ marginTop: "2%" }}
                  onClick={this.handleDelete}
                >
                  Delete Post
                </Button>
              </div>
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}> ©2020 Created by AP</Footer>
        </Layout>
      </div>
    );
  }
}

export default EditBlog;
