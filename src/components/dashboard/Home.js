import React, { Component } from "react";
import { Modal, Button, Card, Layout, Row, Col, Breadcrumb } from "antd";
import { Input } from "antd";
import Blogs from "./Blogs";
import { authHeader } from "../../helpers/auth-headers";
import Axios from "axios";
import { connect } from 'react-redux'

const { Header, Footer, Sider, Content } = Layout;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      visible: false,
      title: "",
      body: "",
      blogs: []
    };
  }

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  handleOk = () => {
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 3000);
  };

  handleCancel = () => {
    this.setState({ visible: false });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  handleSubmit = e => {
    const { title, body } = this.state;
    let bodyitem = JSON.stringify({ blog: { title: title, body: body } });
    fetch("http://localhost:3000/blogs", {
      method: "POST",
      headers: authHeader(),
      body: bodyitem
    })
      .then(response => {
        return response.json();
      })
      .then(blog => {
        this.addnewBlog(blog);
      });
    this.setState({ loading: true });
    setTimeout(() => {
      this.setState({ loading: false, visible: false });
    }, 2000);
  };

  addnewBlog(blog) {
    this.setState({
      blogs: this.state.blogs.concat(blog)
    });
  }

  async componentDidMount() {
    const config = {
      method: "get",
      url: "http://localhost:3000/blogs.json",
      headers: authHeader()
    };
    let blogs = await Axios(config);
    this.setState({
      blogs: blogs.data
    });
  }

  // makeRequest();
  // fetch("http://localhost:3000/blogs.json",{
  //   method: "GET",
  //   headers: authHeader()
  // })
  //   .then(response => {
  //     return response.json();
  //   })
  //   .then(blogs => {
  //     this.setState({
  //       blogs: blogs
  //     });
  //   });
  // };

  passBlogsToFunctionalComponent = () => {
    return this.state.blogs;
  };

  render() {
    const { visible, loading } = this.state;
    const { TextArea } = Input;
    const {blogs}=this.props
    return (
      <div class="container">
        <Row>
          <Button
            type="primary"
            onClick={this.showModal}
            style={{ float: "left" }}
            className="buttonModal"
          >
            Create a Blog
          </Button>
        </Row>
        <Layout>
          <Modal
            visible={visible}
            title="Blog"
            onOk={this.handleOk}
            onCancel={this.handleCancel}
            footer={[
              <Button key="back" onClick={this.handleCancel}>
                Return
              </Button>,
              <Button
                key="submit"
                type="primary"
                loading={loading}
                onClick={this.handleSubmit.bind(this)}
              >
                Submit
              </Button>
            ]}
          >
            <p>
              <TextArea
                onChange={this.handleChange.bind(this)}
                placeholder="Write Title related to Sports or any"
                autoSize
                label="Title"
                hello
                Content
                name="title"
                style={{ margin: "24px 0" }}
                rules={[
                  {
                    required: true,
                    message: "Please input your First Name!"
                  }
                ]}
              />
              <div style={{ margin: "24px 0" }} />
            </p>
            <p>
              <TextArea
                name="body"
                onChange={this.handleChange}
                placeholder="Write Content about Sports or any"
                autoSize={{ minRows: 2, maxRows: 6 }}
              />
              <div style={{ margin: "24px 0" }} />
            </p>
          </Modal>
          <Content style={{ padding: "0 50px" }}>
            <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Blogs</Breadcrumb.Item>
            </Breadcrumb>
            <div className="site-layout-content">
              <Blogs blogs={blogs} />
            </div>
          </Content>
          <Footer style={{ textAlign: "center" }}> ©2020 Created by AP</Footer>
        </Layout>
      </div>
    );
  }
}

const mapStateToProps=(state)=>{
  return{
    blogs: state.blog.blogs
  }
}

export default connect(mapStateToProps)(Home);
