import React, { Component } from "react";
import { Card, Row, Col } from "antd";
import MainBlog from "./MainBlog";

class Blog extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    let totalBlogs = this.props.blogs && this.props.blogs.map(blog => {
      return (
        <Col span={8}>
          <MainBlog blog={blog} key={blog.id} />
        </Col>
      );
    });
    return <Row gutter={16}>{totalBlogs}</Row>;
  }
}

export default Blog;
