import React, { Component } from "react";
import { Card, Row, Col, Modal } from "antd";
import { EyeOutlined, EditOutlined, DeleteOutlined } from "@ant-design/icons";
import { Link } from "react-router-dom";
import ShowBlog from "./EditBlog";
import axios from "axios";

class MainBlog extends Component {
  state = {
    visible: false,
    blog: []
  };

  componentDidMount() {
    this.setState({
      blog: this.props.blog
    });
  }

  convertDate = date => {
  //   let splited = date.split("-");
  //   return splited[0] + "-" + splited[1] + "-" + splited[2][0] + splited[2][1];
  };

  handleOk = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    console.log(e);
    this.setState({
      visible: false
    });
  };

  showModal = () => {
    this.setState({
      visible: true
    });
  };

  render() {
    const { blog } = this.props;
    return (
      <Card title={this.props.blog.title}>
        {this.props.blog.body}
        <Row>
          <Col span={8}></Col>
          <Col span={2}>
            <Link>
              <EyeOutlined onClick={this.showModal} />
            </Link>
            <Modal
              title={blog.title}
              visible={this.state.visible}
              onOk={this.handleOk}
              onCancel={this.handleCancel}
            >
              <p>Published at:{this.convertDate(this.props.blog.created_at)}</p>
              <p></p>
              <p>{blog.body}</p>
            </Modal>
          </Col>
          <Col span={4}>
            <Link
              to={{
                pathname: `blogs/${blog.id}/edit`,
                state: blog
              }}
            >
              <EditOutlined />
            </Link>
          </Col>

          <Col span={10}></Col>
        </Row>
      </Card>
    );
  }
}

export default MainBlog;