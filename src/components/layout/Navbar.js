import React, { Component } from "react";
import { Menu, Icon, Row, Col } from "antd";
import { Link } from "react-router-dom";
import SignOut from "../auth/SignOut";

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class Navbar extends Component {
  render() {
    return (
      <div className="container">
        <div className="brand-logo-text">
          <Row>
            <Col span={10}>
              <Link to="/">SPORT TRACKER</Link>
            </Col>
            <Col span={10}></Col>
            <Col span={4}>
              <Link to="/logout">
                <SignOut />
              </Link>
            </Col>
          </Row>
        </div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["1"]}>
          <Menu.Item key="1">
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item key="5">
            <Link to="/">Games</Link>
          </Menu.Item>
          <Menu.Item key="2" to="/create">
            <Link to="/create">SignUp</Link>
          </Menu.Item>
          <Menu.Item key="3" to="/login">
            <Link to="/login">SignIn</Link>
          </Menu.Item>
          <Menu.Item key="4">
            <Link to="/">LogOut</Link>
          </Menu.Item>
        </Menu>
      </div>
    );
  }
}
export default Navbar;
