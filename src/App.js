import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import Navbar from "./components/layout/Navbar";
import Home from "./components/dashboard/Home";
import Signin from "./components/auth/SignIn";
import SignUp from "./components/auth/SignUp";
import Cookies from "js-cookie";
import EditBlog from "./components/dashboard/EditBlog";
import SignOut from "./components/auth/SignOut";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/home" component={Home} />
          <Route exact path="/login" component={Signin} />
          <Route exact path="/create" component={SignUp} />
          <Route exact path="/blogs/:id/edit" component={EditBlog} />
          <Route exact path="/logout" component={SignOut} />
        </switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
