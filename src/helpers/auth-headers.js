import Cookies from "js-cookie";

export function authHeader() {
  return {
    Accept: "application/json",
    "access-token": Cookies.get("accesstoken"),
    "Content-Type": "application/json", // TODO: verify from request headers
    client: Cookies.get("client"),
    expiry: Cookies.get("expiry"),
    uid: Cookies.get("uid")
  };
}
